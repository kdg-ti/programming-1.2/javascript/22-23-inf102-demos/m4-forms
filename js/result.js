const output = document.getElementById("output")
init();

function handleParameters() {
  output.textContent = new URLSearchParams(location.search).get("date");
}

function init(){
  handleParameters()
}