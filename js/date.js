const dateInput = document.getElementById("date");
const dateTimeInput = document.getElementById("date-time");
const output = document.getElementById("output");
const birthdayButton = document.getElementById("birthday-button");
const TIME_DAY = 1000*60*60*24;

init();

function showBirthday(e) {
  e.preventDefault();
  let birthDay = new Date(dateInput.value);
  const today = new Date();
  birthDay.setFullYear(today.getFullYear());
  output.innerHTML+= `Birthday this year: ${birthDay} <br>`;
  if (birthDay < today){
    birthDay.setFullYear(birthDay.getFullYear()+1);
  }
  output.innerHTML+= `Before your birthday you'll have to wait: ${((birthDay-today)/TIME_DAY).toFixed(0)} days<br>`;



}

function init(){
  for (      i=0;i<5;i++) {console.log(i);}
  dateInput.addEventListener("blur",getDate);
  dateTimeInput.addEventListener("blur",getDate);
  //birthdayButton.addEventListener("click",showBirthday);


}

function getDate(e){

  let dateString = e.target.value;
  output.innerHTML+= `${dateString} <br>`;
  let theDate = new Date(dateString);
  output.innerHTML+= `${theDate} <br>`;
  output.innerHTML+= `${theDate.getTime()} <br>`;
  // output.innerHTML+= dateInput.value + " <br>"
}